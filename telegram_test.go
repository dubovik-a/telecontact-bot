package main

import "testing"

func TestTelegramLogs(t *testing.T) {
	dataItems := []callSessionData{
		{[]string{"numa-test"}, []string{"numb-test"}, []string{"callsessionid-test"}, "starttime-test", "returned-code", "error-test"},

	}

	controlSample := " numa: numa-test" + "\nnumb: numb-test" + "\ncall_session_id: callsessionid-test" + "\nstart_time: starttime-test" + "\nreturned_code: returned-code" + "\nerror: error-test"

	for _, item := range dataItems {
		testLogs := TelegramLogs(item)

		if testLogs != controlSample {
			t.Errorf("TelegramLogs failed\nExpected:\n\n %s\nGot:\n\n %s", controlSample, testLogs)
		}
	}
}
