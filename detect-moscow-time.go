package main

import (
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"
)

type callSessionData struct {
	numa          []string
	numb          []string
	callSessionId []string
	startTime     string
	returnedCode  string
	errLog        string
}

/* send logs to bot */
func TelegramLogs(currentCallData callSessionData) string {
	token := os.Getenv("TELEGRAM_TOKEN")
	chatId := os.Getenv("CHAT_ID")
	if token == "" || chatId == "" {
		err := godotenv.Load(".env")
		if err != nil {
			logrus.Fatalf("Error loading .env file")
		}
		token = os.Getenv("TELEGRAM_TOKEN")
		chatId = os.Getenv("CHAT_ID")
	}

	var allLogs string

	if currentCallData.errLog != "" {
		allLogs = " numa: " + currentCallData.numa[0] + "\nnumb: " + currentCallData.numb[0] + "\ncall_session_id: " + currentCallData.callSessionId[0] + "\nstart_time: " + currentCallData.startTime + "\nreturned_code: " + currentCallData.returnedCode + "\nerror: " + currentCallData.errLog
	} else {
		allLogs = " numa: " + currentCallData.numa[0] + "\nnumb: " + currentCallData.numb[0] + "\ncall_session_id: " + currentCallData.callSessionId[0] + "\nstart_time: " + currentCallData.startTime + "\nreturned_code: " + currentCallData.returnedCode
	}

	Url, err := url.Parse("https://api.telegram.org")
	if err != nil {
		logrus.Fatal("TelegramLogs: ", err)
		panic(err)
	}

	Url.Path += "/bot" + token + "/sendMessage"
	parameters := url.Values{}
	parameters.Add("chat_id", chatId)
	parameters.Add("text", allLogs)
	Url.RawQuery = parameters.Encode()

	req, err := http.NewRequest("POST", Url.String(), nil)
	if err != nil {
		logrus.Fatal("TelegramLogs: ", err)
	}

	req.Header.Set("Accept", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		logrus.Fatal("TelegramLogs: ", err)
	}
	defer resp.Body.Close()
	return allLogs
}

/* GET phone call */
func getPhoneCall(w http.ResponseWriter, r *http.Request) {

	/* structure for response json */
	type Answer struct {
		ReturnedCode int `json:"returned_code"`
	}

	answer := Answer{ReturnedCode: 1}

	/* parse query params */
	numa, ok := r.URL.Query()["numa"]
	numb, ok := r.URL.Query()["numb"]
	callSessionId, ok := r.URL.Query()["call_session_id"]
	startTime, ok := r.URL.Query()["start_time"]

	currentCallData := callSessionData{
		numa,
		numb,
		callSessionId,
		"",
		strconv.Itoa(answer.ReturnedCode),
		"",
	}

	/* check if time exists */
	if !ok || len(startTime[0]) < 0 {
		jsonBytes, err := json.Marshal(answer)
		if err != nil {
			logrus.Fatal("ListenAndServe: ", err)
			currentCallData.errLog = err.Error()
			TelegramLogs(currentCallData)
			w.WriteHeader(http.StatusBadRequest)
			w.Write(jsonBytes)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(jsonBytes)
		return
	}

	/* parse time from unix to utc */
	parsedTime, err := strconv.ParseInt(startTime[0], 10, 64)
	if err != nil {
		currentCallData.startTime = strconv.Itoa(int(parsedTime))
		currentCallData.errLog = err.Error()
		TelegramLogs(currentCallData)
		jsonBytes, err2 := json.Marshal(answer)
		if err2 != nil {
			logrus.Fatal("ListenAndServe: ", err)
			currentCallData.errLog = err.Error()
			TelegramLogs(currentCallData)
			w.WriteHeader(http.StatusBadRequest)
			w.Write(jsonBytes)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(jsonBytes)
		//panic(err)
		return
	}
	tm := time.Unix(parsedTime, 0)

	/* check time interval (hours) */
	if tm.Hour()+3 < 10 && tm.Hour()+3 >= 22 {
		answer = Answer{ReturnedCode: 0}
	}
	jsonBytes, err := json.Marshal(answer)

	/* send logs */
	currentCallData.startTime = tm.String()
	currentCallData.returnedCode = strconv.Itoa(answer.ReturnedCode)
	TelegramLogs(currentCallData)
	logrus.WithFields(logrus.Fields{
		"project":         "detect time interval",
		"package":         "main",
		"function":        "detect-moscow-time.getPhoneCall",
		"numa":            numa,
		"numb":            numb,
		"call_session_id": callSessionId,
		"start_time":      startTime,
		"answer":          answer,
	})

	/* return response */
	w.Header().Add("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonBytes)
}

/* get port from environment */
func getPort() string {
	var port = os.Getenv("PORT")
	if port == "" {
		port = "8080"
		fmt.Println("INFO: No PORT environment variable detected, defaulting to " + port)
	}
	return ":" + port
}

func main() {
	http.HandleFunc("/", getPhoneCall)
	err := http.ListenAndServe(getPort(), nil)
	if err != nil {
		logrus.Fatal("ListenAndServe: ", err)
	}
}
